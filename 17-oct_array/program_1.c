/* WAp which will take 2 arrays from the user and compare between them. */

#include<stdio.h>
void main(){

	int flag=0;

	int size1,size2;
	puts("Enter the size of array 1");
	scanf("%d",&size1);

	puts("Enter the size of array 2");
	scanf("%d",&size2);

	int arr1[size1];
	int arr2[size2];

	puts("Enter elements of array 1");
	for(int i=0;i<size1;i++){
	
		scanf("%d",&arr1[i]);
	}

	puts("Enter the elements of array 2");
	for(int i=0;i<size2;i++){
	
		scanf("%d",&arr2[i]);
	
	}


	for(int i=0;i<size1;i++){
	
		if(arr1[i]==arr2[i]){
		
			flag=0;
		
		}
		else
			flag++;
	
	}
	if(flag==0)
		puts("arrays are equal.");

	else
		puts("Arrays are not equal.");


	puts("The elements are:");
	for(int i=0;i<size1;i++){
	
		printf("%d   %d ",arr1[i],arr2[i]);
		printf("\n");
	
	
	}
}
