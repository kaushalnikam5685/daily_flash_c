/* WAp to make an array of the table of given number print the array using a pointer .  */

#include<stdio.h>
void main(){

	int size;
	int num;

	puts("Enter the size of array");
	scanf("%d",&size);

	int arr[size];

	puts("Enter the Number:");
	scanf("%d",&num);

	for(int i=1;i<=size;i++){
	        
		arr[i]=num*i;
	}


	puts("The elements are:");
	for(int i=1;i<=size;i++){
	
		printf("%d",*(arr+i));
		printf("\n");
	
	
	}
}
